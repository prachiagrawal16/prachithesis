package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddResponseController {

    @FXML
    private TextField responsename;

    @FXML
    private TextField responseabbr;

    @FXML
    private TextField responseunit;

    @FXML
    private TextField responselow;

    @FXML
    private TextField responsehigh;

    @FXML
    private RadioButton controlledresponse;

    @FXML
    private RadioButton uncontrolledresponse;

    @FXML
    private RadioButton constantresponse;

    @FXML
    private Button canceladdresponse;

    @FXML
    void addresponseok(ActionEvent event) {
    	String newfactorname = responsename.getText();
    	String newfactorabbr = responseabbr.getText();
    	String newfactorunit = responseunit.getText();
    	String newfactorlowvalue = responselow.getText();
    	String newfactorhighvalue = responsehigh.getText();
    }

    @FXML
    void cancellingaddresponse(ActionEvent event) {
    	Stage stage = (Stage) canceladdresponse.getScene().getWindow();
        stage.close();
    }

    @FXML
    void constantresponserole(ActionEvent event) {

    }

    @FXML
    void controlledresponserole(ActionEvent event) {

    }

    @FXML
    void responseabbrfield(ActionEvent event) {

    }

    @FXML
    void responsehighvalue(ActionEvent event) {

    }

    @FXML
    void responselowvalue(ActionEvent event) {

    }

    @FXML
    void responsenamefield(ActionEvent event) {

    }

    @FXML
    void responseunitfield(ActionEvent event) {

    }

    @FXML
    void uncontrolledresponserole(ActionEvent event) {

    }

}

