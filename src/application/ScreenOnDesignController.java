package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ScreenOnDesignController implements Initializable{

    @FXML
    private AnchorPane rootpane;

    @FXML
    private Button designButton;

    @FXML
    private Button worksheetbutton;     

    @FXML
    private Button analysebutton;

    @FXML
    private Button predictbutton;

    @FXML
    private Button addfactorbutton;

    @FXML
    private Button removefactorbutton;
    

    @FXML
    private Button modifyfactorbutton;
    
    @FXML
    private Button generateDoe;

    @FXML
    private Button cancelthedesign;
    
    @FXML
    private TableView<Details> factortable;

    @FXML
    private TableColumn<Details, String> factorname;

    @FXML
    private TableColumn<Details, String> factorabbr;

    @FXML
    private TableColumn<Details, String> factorrole;

    @FXML
    private TableColumn<?, ?> factortransform;

    @FXML
    private TableColumn<?, ?> factorgradient;

    @FXML
    private TableColumn<?, ?> factoroffset;

    @FXML
    private TableColumn<?, ?> factorlow;

    @FXML
    private TableColumn<?, ?> factorhigh;

    @FXML
    private Button addresponsebutton;

    @FXML
    private Button removeresponsebutton;
    
    @FXML
    private Button modifyresponsebutton;

    @FXML
    private TableColumn<?, ?> responsename;

    @FXML
    private TableColumn<?, ?> responseabbr;

    @FXML
    private TableColumn<?, ?> responserole;

    @FXML
    private TableColumn<?, ?> responsetransform;

    @FXML
    private TableColumn<?, ?> responsegradient;

    @FXML
    private TableColumn<?, ?> responseoffset;

    @FXML
    private TableColumn<?, ?> responselow;

    @FXML
    private TableColumn<?, ?> responsehigh;

 
    public ObservableList<Details> detaillist = FXCollections.observableArrayList(
    		new Details("Mass", "kg", "kg"),
    		new Details("Gravity", "m", "m")
    );
    
   

	@FXML
    void addfactorhandleevent(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AddFactor.fxml"));
    	Parent root1 = (Parent)loader.load();
    	Stage stage = new Stage();
    	stage.setTitle("Add New Factor");
    	stage.setScene(new Scene(root1));
    	stage.show();
    }

    @FXML
    void addresponsehandleevent(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AddResponse.fxml"));
    	Parent root1 = (Parent)loader.load();
    	Stage stage = new Stage();
    	stage.setTitle("Add New Response");
    	stage.setScene(new Scene(root1));
    	stage.show();
    }

    @FXML
    void analysehandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnAnalyse.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void designhandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnDesign.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void predicthandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnPredict.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void worksheethandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnWorksheet.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void factornamehandle(ActionEvent event) {

    }

    @FXML
    void factortablehandleevent(ActionEvent event) {

    }
    
    @FXML
    void removefactorhandleevent(ActionEvent event) {

    }

    @FXML
    void removeresponsehandleevent(ActionEvent event) {

    }
    
    @FXML
    void modifyfactorhandleevent(ActionEvent event) {

    }

    @FXML
    void modifyresponsehandleevent(ActionEvent event) {

    }
    
    @FXML
    void cancelthedesignhandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("Screen1.fxml"));
    	rootpane.getChildren().setAll(pane);
    }
    
    @FXML
    void generateDoehandleevent(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("GenerateDoE.fxml"));
    	Parent root1 = (Parent)loader.load();
    	Stage stage = new Stage();
    	stage.setTitle("Select the model");
    	stage.setScene(new Scene(root1));
    	stage.show();
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		factorname.setCellValueFactory(new PropertyValueFactory<Details, String>("factorname"));
		factorabbr.setCellValueFactory(new PropertyValueFactory<Details, String>("factorabbr"));
		factorrole.setCellValueFactory(new PropertyValueFactory<Details, String>("factorrole"));
		factortable.setItems(detaillist);
	}
    
}
