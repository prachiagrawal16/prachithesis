package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddFactorController {

    @FXML
    private TextField factorname;

    @FXML
    private TextField factorabbr;

    @FXML
    private TextField factorunit;

    @FXML
    private TextField factorlow;

    @FXML
    private TextField factorhigh;

    @FXML
    private RadioButton controlledfactor;

    @FXML
    private RadioButton uncontrolledfactor;

    @FXML
    private RadioButton constantfactor;

    @FXML
    private Button canceladdfactor;

    @FXML
    void addfactorok(ActionEvent event) {
    	String newfactorname = factorname.getText();
    	String newfactorabbr = factorabbr.getText();
    	String newfactorunit = factorunit.getText();
    	String newfactorlowvalue = factorlow.getText();
    	String newfactorhighvalue = factorhigh.getText();
    	System.out.println(newfactorname);
    	System.out.println(newfactorabbr);
    	System.out.println(newfactorunit);
    	System.out.println(newfactorlowvalue);
    	System.out.println(newfactorhighvalue);
    }

    @FXML
    void cancellingaddfactor(ActionEvent event) {
    	Stage stage = (Stage) canceladdfactor.getScene().getWindow();
        stage.close();
    }

    @FXML
    void constantfactorrole(ActionEvent event) {

    }

    @FXML
    void controlledfactorrole(ActionEvent event) {

    }

    @FXML
    void factorabbrfield(ActionEvent event) {

    }

    @FXML
    void factorhighvalue(ActionEvent event) {

    }

    @FXML
    void factorlowvalue(ActionEvent event) {

    }

    @FXML
    void factornamefield(ActionEvent event) {

    }

    @FXML
    void factorunitfield(ActionEvent event) {

    }

    @FXML
    void uncontrolledfactorrole(ActionEvent event) {

    }

}
