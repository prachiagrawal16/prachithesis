package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

public class Screen1Controller {
	
	@FXML
	private AnchorPane rootpane;

    @FXML
    private Button designButton;

    @FXML
    private Button worksheetbutton;

    @FXML
    private Button analysebutton;

    @FXML
    private Button predictbutton;

    @FXML
    void analysehandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnAnalyse.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void designhandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnDesign.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void predicthandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnPredict.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

    @FXML
    void worksheethandleevent(ActionEvent event) throws IOException {
    	AnchorPane pane = FXMLLoader.load(getClass().getResource("ScreenOnWorksheet.fxml"));
    	rootpane.getChildren().setAll(pane);
    }

}
