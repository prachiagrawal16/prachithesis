package application;

import javafx.beans.property.SimpleStringProperty;

public class Details {
	private final SimpleStringProperty name;
	private final SimpleStringProperty abbr;
	private final SimpleStringProperty role;
	
	public Details(String name, String abbr, String role) {
		super();
		this.name = new SimpleStringProperty(name);
		this.abbr = new SimpleStringProperty(abbr);
		this.role = new SimpleStringProperty(role);
	}

	public String getName() {
		return name.get();
	}

	public String getAbbr() {
		return abbr.get();
	}

	public String getRole() {
		return role.get();
	}
	
}
