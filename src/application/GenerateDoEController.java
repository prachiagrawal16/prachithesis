package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class GenerateDoEController {

    @FXML
    private Button cancelbutton;

    @FXML
    private Button okbutton;

    @FXML
    void cancelthemodelchosen(ActionEvent event) {
    	Stage stage = (Stage) cancelbutton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void okbuttonhandleevent(ActionEvent event) {

    }

}
